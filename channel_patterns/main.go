package main

import (
	"fmt"
	"runtime"
	"sync/atomic"
)

func main() {
	printFromClose()
}

type spinLock struct {
	state *int64
}
type ticketStorage struct {
	ticket *uint64
	done   *uint64
	slots  []string //infinite-like
}

const free = int64(0)

//пустой канал при чтении возвращает дефолтное значение и булеан
func printFromClose() {
	c := make(chan int)
	close(c)
	val, boolean := <-c
	fmt.Println(val, boolean)
}

//костыль с действием если канал заблочен
func nonBlockReceive(c <-chan int) (result int, more, ok bool) {
	select {
	case data, more := <-c:
		return data, more, true
	default:
		return 0, true, false
	}
}

// 1 в несколько(посылаем в первый незалоченный канал)
func massSend(in <-chan int, outOne, outTwo chan int) {
	for i := range in {
		select {
		case outOne <- i:
		case outTwo <- i:
		}
	}
}

func manyInMany(disrupt <-chan int, inOne, inTwo, outOne, outTwo chan int) {
	var data int
	for {
		select {
		//read from first opened
		case data = <-inOne:
		case data = <-inTwo:
		//its like antipattern but ok(?)
		case <-disrupt:
			close(inOne)
			close(inTwo)
			//flush data
			massSend(inOne, outOne, outTwo)
			massSend(inTwo, inOne, inTwo)
		}
	}
	fmt.Println(data) //unreachable
}

//spinLock
func (s *spinLock) spinningCASLock() {
	for !atomic.CompareAndSwapInt64(s.state, free, 1) {
		runtime.Gosched()
	}
}

//spinUnlock
func (s *spinLock) spinningCASUnlock() {
	atomic.StoreInt64(s.state, free)
}

//ticketStorage
func (ts *ticketStorage) ticketStoragePut(s string) {
	t := atomic.AddUint64(ts.ticket, 1) - 1
	ts.slots[t] = s
	//synchronises done value
	for !atomic.CompareAndSwapUint64(ts.done, t, t+1) {
		runtime.Gosched()
	}
}

func (ts *ticketStorage) ticketStorageGetDone() []string {
	return ts.slots[:atomic.LoadUint64(ts.done)+1] //read all up to done
}

/* TIPS
avoid blocking
avoid RK
select to switch between channel
using channel to avoid shared values
if channels dont work -> synchronized package ->lockless(atomic)
*/
