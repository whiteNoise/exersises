package main

import (
	"sort"
	"strconv"
	"strings"
)

//сюда вам надо писать функции, которых не хватает, чтобы проходили тесты в gotchas_test.go

func IntSliceToString(values []int) string {
	valuesText := []string{}
	for i := range values {
		number := values[i]
		text := strconv.Itoa(number)
		valuesText = append(valuesText, text)
	}

	// Join our string slice.
	result := strings.Join(valuesText, "")
	return result
}

func MergeSlices(a []float32, b []int) []int {
	var result []int
	for _, num := range a {
		result = append(result, int(num))
	}
	for _, num := range b {
		result = append(result, int(num))
	}
	return result
}

func GetMapValuesSortedByKey(a map[int]string) []string {
	var keys []int
	var result []string
	for k := range a {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, k := range keys {
		result = append(result, a[k])
	}
	return result
}
