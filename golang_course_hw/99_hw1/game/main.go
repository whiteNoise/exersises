package main

import (
	"fmt"
	"strings"
)

type Room struct {
	items     []*Item
	player    *Person
	name      string
	backpack  *Backpack
	nextRooms []*Room
}

type Person struct {
	location *Room
	backpack *Backpack
}
type Item struct {
	name string
	used bool
}
type Backpack struct {
	name          string
	storage       []*Item
	worn          bool
	matchingItems map[string]string
}

type Location struct {
	rooms []*Room
}

func (l *Location) makeNewRoom(s string) {
	c := new(Room)
	l.rooms = append(l.rooms, c)
}

func (p *Person) pickBackpack() string {
	if p.location.backpack != nil {
		p.backpack = p.location.backpack
		p.location.backpack = nil
		p.backpack.worn = true
		return "вы надели: рюкзак"
	}
	return "рюкзака нет в комнате"

} // взять рюкзак

func (r *Room) placed() string {
	if r.backpack != nil {
		return " на стуле: рюкзак."
	}
	return ""
}

func (p *Person) moveTo(r2 string) string {
	for _, room := range p.location.nextRooms {
		if room.name == "улица" && Door.used == false {
			return view(room, trace)
		}
		if room.name == r2 {
			room.player = p
			p.location = room
			p.location.player = nil
			return view(room, trace) + room.printAvailableRooms()
		}
	}
	return "нет пути в " + r2
} // перемещает игрока

func (r *Room) printAvailableRooms() string {
	var s string
	for _, room := range r.nextRooms {
		s += room.name + ", "
	}
	s = strings.TrimSuffix(s, ", ")
	return "можно пройти - " + s
} // куда можно пройти

func view(r *Room, operation func(*Room) string) string {
	result := operation(r)
	return result
} // выдает описание конкретной комнаты

func trace(r *Room) string {
	switch r.name {
	case "комната":
		return "ты в своей комнате. "
	case "кухня":
		return "кухня, ничего интересного. "
	case "коридор":
		return "ничего интересного. "
	case "улица":
		if Door.used == true {
			return "на улице весна. "
		}
		return "дверь закрыта"
	}
	return ""
} // описание комнат для move

func (r *Room) printItemsInfo() string {
	var s string
	for _, e := range r.items {
		s += e.name + ", "
	}
	s = strings.TrimSuffix(s, ", ")
	return s
} //форматирует список предметов комнаты

func look(r *Room) string {
	switch r.name {
	case "комната":
		if r.printItemsInfo() == "" && r.placed() == "" {
			return "пустая комната. " + "" + r.printAvailableRooms()
		} else if r.printItemsInfo() != "" && r.placed() == "" {
			return "на столе: " + r.printItemsInfo() + "." + " " + r.printAvailableRooms()
		}
		return "на столе: " + r.printItemsInfo() + "," + r.placed() + " " + r.printAvailableRooms()
	case "кухня":
		if Player1.backpack.worn == false {
			return "ты находишься на кухне, на столе: " + r.printItemsInfo() + ", надо собрать рюкзак и идти в универ." + " " + r.printAvailableRooms()
		}
		return "ты находишься на кухне, на столе: " + r.printItemsInfo() + ", надо идти в универ. " + r.printAvailableRooms()
	}
	return ""
} // для осмотреться

func (p *Person) pickItem(name string) string {
	for i, e := range p.location.items {
		if e.name == name && p.backpack.worn == true {
			p.backpack.storage = append(p.backpack.storage, e)
			p.location.items = append(p.location.items[:i], p.location.items[i+1:]...)
			return "предмет добавлен в инвентарь: " + e.name
		} else if p.backpack.worn == false || p.backpack == nil {
			return "некуда класть"
		}
	}

	return "нет такого"
} //проверяем,надет ли рюкзак и добавляем в инвентарь

func (p *Person) useMatchningItems(s1, s2 string) string {
	switch s1 {
	case "ключи":
		switch s2 {
		case "дверь":
			Door.used = true
			return "дверь открыта"
		}
	}
	return ""
} //вывод сообщения если предметы применимы друг к другу

func (p *Person) useItem(s1, s2 string) string {
	for _, item := range p.backpack.storage {
		if item.name == s1 {
			for name1, name2 := range p.backpack.matchingItems { //проходим по таблице предметов которые могут взаимодействовать
				if s1 == name1 && s2 == name2 {
					return p.useMatchningItems(item.name, s2)
				}
				return "не к чему применить"
			}
		}
	}
	return "нет предмета в инвентаре - " + s1
} //применить предмет

var Backpack1 Backpack = Backpack{
	storage: make([]*Item, 3, 3),
	worn:    false,
	name:    "рюкзак",
}

var Player1 Person = Person{
	backpack: &Backpack1,
}

var YourRoom Room = Room{
	player:   &Player1,
	name:     "комната",
	backpack: &Backpack1,
}

var Kitchen Room = Room{
	player:   &Player1,
	name:     "кухня",
	backpack: &Backpack1,
}

var Hall Room = Room{
	name: "коридор",
}

var Outside Room = Room{
	name: "улица",
}

var Home Room = Room{
	name: "домой",
}

var Tea Item = Item{
	name: "чай",
}
var Conspects Item = Item{
	name: "конспекты",
}
var Key Item = Item{
	name: "ключи",
}
var Door Item = Item{
	name: "дверь",
}
var Phone Item = Item{
	name: "телефон",
}

func main() {
	initGame()
	fmt.Println(Player1.location.name)
	fmt.Println(Player1.moveTo("коридор"))
	fmt.Println(Player1.location.name)
	fmt.Println(Player1.moveTo("улица"))
	fmt.Println(Player1.location.name)
	fmt.Println(Player1.moveTo("комната"))
	fmt.Println(Player1.location.name)
	fmt.Println(Player1.moveTo("коридор"))
	fmt.Println(Player1.moveTo("кухня"))
	fmt.Println(Kitchen.printItemsInfo())
	fmt.Println(view(Player1.location, look))

	fmt.Println(Player1.pickBackpack())
	fmt.Println(view(&Kitchen, look))

}
func initGame() {
	Backpack1.worn = false
	Backpack1.storage = []*Item{}
	Backpack1.matchingItems = map[string]string{
		"ключи": "дверь",
	}
	Door.used = false

	Player1.location = &Kitchen
	YourRoom.backpack = &Backpack1

	Kitchen.nextRooms = []*Room{&Hall}
	Hall.nextRooms = []*Room{&Kitchen, &YourRoom, &Outside}
	YourRoom.nextRooms = []*Room{&Hall}
	Outside.nextRooms = []*Room{&Home}

	YourRoom.items = []*Item{&Key, &Conspects}
	Kitchen.items = []*Item{&Tea}
}
func handleCommand(str string) string {
	s := strings.Split(str, " ")
	switch s[0] {
	case "идти":
		return Player1.moveTo(s[1])
	case "осмотреться":
		return view(Player1.location, look)
	case "взять":
		return Player1.pickItem(s[1])
	case "применить":
		return Player1.useItem(s[1], s[2])

	case "надеть":
		return Player1.pickBackpack()
	}
	return "неизвестная команда"
}
