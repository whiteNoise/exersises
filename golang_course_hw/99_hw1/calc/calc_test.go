package main

import (
	"reflect"
	"strconv"
	"testing"
)

// сюда писать тесты
/*
Arithmetic and input tests
*/
func TestArithmetic(t *testing.T) { //pass
	input := []string{"3", "5", "+", "="}
	expected := 9
	result, err := Count(input)
	if !(reflect.DeepEqual(result, expected) || err == nil) {
		t.Error("expected", expected, "have", result, err)
	}
}

func TestArithmetic2(t *testing.T) { //pass
	input := []string{"3", "5", "-", "="}
	expected := -2
	result, err := Count(input)
	if !(reflect.DeepEqual(result, expected) || err == nil) {
		t.Error("expected", expected, "have", result, err)
	}
}

func TestArithmetic3(t *testing.T) { //pass
	input := []string{"3", "5", "*", "="}
	expected := 15
	result, err := Count(input)
	if !(reflect.DeepEqual(result, expected) || err == nil) {
		t.Error("expected", expected, "have", result, err)
	}
}

func TestArithmetic4(t *testing.T) { //pass
	input := []string{"3", "0", "*", "="}
	expected := 0
	result, err := Count(input)
	if !(reflect.DeepEqual(result, expected) || err == nil) {
		t.Error("expected", expected, "have", result, err)
	}
}

func TestArithmetic5(t *testing.T) { //pass
	input := []string{"-2", "0", "*", "="}
	expected := 15
	result, err := Count(input)
	if !(reflect.DeepEqual(result, expected) || err == nil) {
		t.Error("expected", expected, "have", result, err)
	}
}
func TestWrongInput(t *testing.T) { //pass
	_, expected := strconv.Atoi("a")
	input := []string{"3", "a", "*", "="}
	result, err := Count(input)
	if !reflect.DeepEqual(err, expected) {
		t.Error("expected: ", expected, "have :", result, err)
	}
}
