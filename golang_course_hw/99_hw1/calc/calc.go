package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// сюда писать код
// фукция main тоже будет тут

/*
Infinite loop calc with stack
Zero as default result ex. "=" gives back 0 and nil
*/

type stack struct {
	s []int // change to slice?

}

func NewStack() *stack {
	return &stack{make([]int, 0)}
}

func (s *stack) Push(v int) {
	s.s = append(s.s, v)
}

func (s *stack) Empty() bool {
	res := len(s.s)
	return res == 0
}

func (s *stack) Pop() (int, error) {
	l := len(s.s)

	if l == 0 {
		return 0, errors.New("Empty Stack")
	}

	res := s.s[l-1]
	s.s = s.s[:l-1]
	return res, nil
}

func Format() []string {
	reader := bufio.NewReader(os.Stdin)
	input, _, _ := reader.ReadLine() //read line from console ([]byte)
	s := string(input[:])            //turn []byte into string
	s2 := strings.Split(s, " ")      //split string between " "
	return s2
}

func Count(s []string) (int, error) {
	stack := NewStack()
	for _, elem := range s { //for each element in string(expecting number)

		switch elem {
		case "\n":
		case " ":
			break
		case "=":
			res, _ := stack.Pop()
			return res, nil
		case "+":
			num1, _ := stack.Pop()
			num2, _ := stack.Pop()
			stack.Push(num1 + num2)
			break

		case "-":
			num1, _ := stack.Pop()
			num2, _ := stack.Pop()
			stack.Push(num2 - num1)
			break
		case "*":
			num1, _ := stack.Pop()
			num2, _ := stack.Pop()
			stack.Push(num1 * num2)
			break
		default:
			if num, err := strconv.Atoi(elem); err != nil {
				return num, err
			} else {
				stack.Push(num)
			}
			break
		}

	}
	return 0, nil
}

func main() {
L:
	for {
		input := Format()
		res, err := Count(input)
		if err != nil {
			fmt.Println(err)
			break L
		} else {
			fmt.Println(res)
			fmt.Println(err)
		}
	}
}
