package main

import (
	"encoding/json"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
)

var jsonFunc = `

type CustomResponse struct {
	CustomError    string      "json:\"error\""
	CustomResponse interface{} "json:\"response,omitempty\"" //for ommiting empty answers
}

//proper JSON writing
func CustomWriteJSON(w http.ResponseWriter, finalMap map[string]interface{}) {

	custom := CustomResponse{}

	if ok := finalMap["errApi"]; ok != nil {
		customErr, _ := finalMap["errApi"].(ApiError)
		w.WriteHeader(customErr.HTTPStatus)
		custom.CustomError = customErr.Error()
	} 

	if ok := finalMap["err"]; ok != nil {
		w.WriteHeader(500)
		custom.CustomError = fmt.Sprintf("%v", finalMap["err"])
	} 
	
	if ok := finalMap["response"]; ok != nil {
		custom.CustomResponse = finalMap["response"]
	}

	result, err := json.Marshal(custom)
	
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("json error"))
	}
	w.Write(result)
}
`

type Tag struct {
	Url    string
	Auth   bool
	Method string
}

func main() {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, "api.go", nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}
	out, _ := os.Create(os.Args[2])
	fmt.Fprintln(out, `package `+node.Name.Name)
	fmt.Fprintln(out)
	fmt.Fprintln(out, "//CODE GENERATED AUTOMATICALLY , TOUCH IT GENTLY ! ")
	fmt.Fprintln(out)
	fmt.Fprintln(out, "import (")
	fmt.Fprintln(out, "\"encoding/json\"")
	fmt.Fprintln(out, "\"fmt\"")
	fmt.Fprintln(out, "\"net/http\"")
	fmt.Fprintln(out, "\"strconv\"")
	fmt.Fprintln(out, ")")

	fmt.Fprintln(out, jsonFunc)
	fmt.Fprintln(out)
	funct, structs := showInfo(*node)
	startGenerate(out, funct, structs)
}

func showInfo(node ast.File) (map[string][]ast.FuncDecl, map[string]ast.TypeSpec) {

	structures := make(map[string]ast.TypeSpec)
	functions := make(map[string][]ast.FuncDecl)

	for _, elem := range node.Decls {
		funcDeclararion, ok := elem.(*ast.FuncDecl)
		if !ok {
			elem, ok := elem.(*ast.GenDecl)
			if ok {
				for _, spec := range elem.Specs {
					currType, ok := spec.(*ast.TypeSpec)
					if ok {
						_, ok := currType.Type.(*ast.StructType)
						if ok {
							structures[currType.Name.Name] = *currType
						}
					}
				}
			}
		} else {
			if funcDeclararion.Doc != nil {
				strname := funcDeclararion.Recv.List[0].Type.(*ast.StarExpr).X.(*ast.Ident).Name
				functions[strname] = append(functions[strname], *funcDeclararion)
			}
		}
	}
	return functions, structures
}

func startGenerate(out *os.File, functs map[string][]ast.FuncDecl, structs map[string]ast.TypeSpec) {

	for namestruct, decls := range functs {
		for _, elem := range decls {
			for _, nameHandle := range elem.Type.Params.List {
				needstr, ok := nameHandle.Type.(*ast.Ident)
				if ok {
					FillFunc(out, structs[needstr.Name])
					ValidateFunc(out, structs[needstr.Name])
				}
			}
			CustomHandle(out, elem, namestruct)
		}
		ServeHTTP(out, decls, namestruct)
	}
}

func ParseTags(field ast.Field) map[string]interface{} {

	tagMap := make(map[string]interface{})
	fieldTags := reflect.StructTag(field.Tag.Value[1 : len(field.Tag.Value)-1])
	fields := fieldTags.Get("apivalidator")

	for _, item := range strings.Split(fields, ",") {
		if item == "required" {
			tagMap[item] = true
			continue
		} else {
			values := strings.Split(item, "=")
			if values[0] == "enum" {
				tagMap[values[0]] = strings.Split(values[1], "|")
			} else {
				tagMap[values[0]] = values[1]
			}
		}
	}
	return tagMap
}

func CustomHandle(out *os.File, elem ast.FuncDecl, nameHandle string) {

	nameElem := elem.Name.Name
	namefunc := nameElem + "Handler"

	for _, namest := range elem.Type.Params.List {
		needstr, ok := namest.Type.(*ast.Ident)
		if ok {
			fmt.Fprintf(out, "\n// %s for %s\n", namefunc, nameHandle)
			fmt.Fprintf(out, `func (srv *%s) %s (w http.ResponseWriter, r *http.Request) {

	params := %s{}
	finalMap := make(map[string]interface{})
	customErr := params.Fill(r)
	

	if customErr.Err == nil {

		customErr = params.Validate()
		if customErr.Err != nil {
			finalMap["errApi"] = customErr
		} 

		if customErr.Err == nil {
			result, err := srv.%s(r.Context(), params)
			if err != nil {
				if _, ok := err.(ApiError); ok {
					finalMap["errApi"] = err
				} else {
					finalMap["err"] = err
				}
			} else {
				finalMap["response"] = result
			}
		}
	}

	if customErr.Err != nil {
		finalMap["errApi"] = customErr
	} 
	CustomWriteJSON(w, finalMap)
}
`, nameHandle, namefunc, needstr.Name, nameElem)
		}
	}
}

func ValidateFunc(out *os.File, strc ast.TypeSpec) {

	fmt.Fprintln(out, `// Validation function for `+strc.Name.Name)
	fmt.Fprintf(out, "func (object *%s) Validate() ApiError{\n", strc.Name.Name)
	for _, field := range strc.Type.(*ast.StructType).Fields.List {
		fieldTag := ParseTags(*field)
		var pName string
		for _, name := range field.Names {
			if ok := fieldTag["paramname"]; ok == nil {
				pName = strings.ToLower(name.Name)
			} else {
				pName, _ = fieldTag["paramname"].(string)
			}
			if ok := fieldTag["default"]; ok != nil {
				fmt.Fprintf(out, "	if object.%s == \"\" {\n		object.%s = \"%s\"\n	}\n", name.Name, name.Name, fieldTag["default"])
			}
			if ok := fieldTag["required"]; ok != nil && ok == true {
				fmt.Println(out)
				fmt.Fprintf(out,
					`	if object.%s == "" {
				return ApiError {
				HTTPStatus: http.StatusBadRequest,
				Err:        fmt.Errorf("%s must me not empty"),
			}
		}
`, name.Name, pName)
			}
			if ok := fieldTag["min"]; ok != nil {
				min, _ := strconv.Atoi(fieldTag["min"].(string))
				if field.Type.(*ast.Ident).Name == "int" {
					fmt.Fprintf(out,
						`	if object.%s < %d {
					return ApiError {
					HTTPStatus: http.StatusBadRequest,
					Err:        fmt.Errorf("%s must be >= %d"),
				}
			}
`, name.Name, min, pName, min)
				} else {
					fmt.Fprintf(out,
						`	if len(object.%s) < %d {
					return ApiError {
					HTTPStatus: http.StatusBadRequest,
					Err:        fmt.Errorf("%s len must be >= %d"),
				}
			}
`, name.Name, min, pName, min)
				}
			}
			if ok := fieldTag["max"]; ok != nil {
				max, _ := strconv.Atoi(fieldTag["max"].(string))
				if field.Type.(*ast.Ident).Name == "int" {
					fmt.Fprintf(out,
						`	if object.%s > %d {
					return ApiError {
					HTTPStatus: http.StatusBadRequest,
					Err:        fmt.Errorf("%s must be <= %d"),
				}
			}
`, name.Name, max, pName, max)
				} else {
					fmt.Fprintf(out,
						`	if len(object.%s) > %d {
					return ApiError {
					HTTPStatus: http.StatusBadRequest,
					Err: fmt.Errorf("%s len must be <= %d"),
				}
			}
`, name.Name, max, pName, max)
				}
			}
			if ok := fieldTag["enum"]; ok != nil {
				sl, _ := fieldTag["enum"].([]string)
				fmt.Fprintf(out, "	classes := make(map[string]bool)\n")
				for _, val := range sl {
					fmt.Fprintf(out, "	classes[\"%s\"] = true\n", val)
				}
				fmt.Println(out)
				fmt.Fprintf(out,
					`	if !classes[object.%s] {
				return ApiError {
				HTTPStatus: 400,
				Err:        fmt.Errorf("%s must be one of [%s]"),
			}
		}
`, name.Name, pName, strings.Join(sl, ", "))
			}
		}
	}
	fmt.Fprintln(out, `	return ApiError {
		HTTPStatus: 0,
		Err:        nil,
	}
}`)
}

func FillFunc(out *os.File, strc ast.TypeSpec) {
	fmt.Fprintln(out)
	fmt.Fprintln(out, `// Filling func for `+strc.Name.Name)
	fmt.Fprintf(out, "func (object *%s) Fill(r *http.Request) ApiError {\n", strc.Name.Name)
	fmt.Println(out)
	for _, field := range strc.Type.(*ast.StructType).Fields.List {

		fieldTag := ParseTags(*field)
		var pName string

		for _, name := range field.Names {
			if ok := fieldTag["paramname"]; ok == nil {
				pName = strings.ToLower(name.Name)
			} else {
				pName, _ = fieldTag["paramname"].(string)
			}
			if field.Type.(*ast.Ident).Name == "string" {
				fmt.Fprintf(out, "	object.%s = r.FormValue(\"%s\")\n", name.Name, pName)
			} else {
				fmt.Fprintf(out,
					`  	%s, err := strconv.Atoi(r.FormValue("%s"))
	if err != nil {
		return ApiError {
		HTTPStatus: http.StatusBadRequest,
		Err:        fmt.Errorf("%s must be int"),
		}
	} else {
		object.%s = %s
	}
`, pName, pName, pName, name.Name, pName)
			}
		}
	}
	fmt.Fprintln(out, `	return ApiError {
		HTTPStatus: 0,
		Err:        nil,
	}
}`)
}

func ServeHTTP(out *os.File, decls []ast.FuncDecl, nameHandle string) {
	fmt.Fprintf(out, "// HTTP wrapper for %s\n", nameHandle)
	fmt.Fprintf(out, `func (srv *%s) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	
	finalMap := make(map[string]interface{})
	write := true

	switch r.URL.Path {
`, nameHandle)

	fmt.Println(out)

	var tag Tag
	var strTags string

	for _, elem := range decls {
		if strings.HasPrefix(elem.Doc.List[0].Text, "// apigen:api ") {
			strTags = elem.Doc.List[0].Text[len("// apigen:api "):]
			_ = json.Unmarshal([]byte(strTags), &tag)
			fmt.Fprintf(out, "	case \"%s\":\n", tag.Url)
			fmt.Println(out)
			if tag.Method != "" {
				fmt.Fprintf(out,
					` if r.Method != "%s" {

			finalMap["errApi"] = ApiError {
			HTTPStatus: 406,
			Err: fmt.Errorf("bad method"),
			}
			break
		}
`, tag.Method)
			}

			if tag.Auth {
				fmt.Fprintf(out,
					` if r.Header.Get("X-Auth") != "100500" {
						
			finalMap["errApi"] = ApiError {
			HTTPStatus: 403,
			Err: fmt.Errorf("unauthorized"),
			}
			break
		} 
`)
			}

			fmt.Fprintf(out,
				` srv.%s(w, r)
		write = false
		break
`, elem.Name.Name+"Handler")
		}
	}
	fmt.Fprintln(out, `	
	default:

		finalMap["errApi"] = ApiError {
		HTTPStatus: 404,
		Err: fmt.Errorf("unknown method"),
		}
	}
	if write {
		CustomWriteJSON(w, finalMap)
	}
}`)
}
