package main

//CODE GENERATED AUTOMATICALLY , TOUCH IT GENTLY !

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

type CustomResponse struct {
	CustomError    string      "json:\"error\""
	CustomResponse interface{} "json:\"response,omitempty\"" //for ommiting empty answers
}

//proper JSON writing
func CustomWriteJSON(w http.ResponseWriter, finalMap map[string]interface{}) {

	custom := CustomResponse{}

	if ok := finalMap["errApi"]; ok != nil {
		customErr, _ := finalMap["errApi"].(ApiError)
		w.WriteHeader(customErr.HTTPStatus)
		custom.CustomError = customErr.Error()
	}

	if ok := finalMap["err"]; ok != nil {
		w.WriteHeader(500)
		custom.CustomError = fmt.Sprintf("%v", finalMap["err"])
	}

	if ok := finalMap["response"]; ok != nil {
		custom.CustomResponse = finalMap["response"]
	}

	result, err := json.Marshal(custom)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("json error"))
	}
	w.Write(result)
}

// Filling func for ProfileParams
func (object *ProfileParams) Fill(r *http.Request) ApiError {
	object.Login = r.FormValue("login")
	return ApiError{
		HTTPStatus: 0,
		Err:        nil,
	}
}

// Validation function for ProfileParams
func (object *ProfileParams) Validate() ApiError {
	if object.Login == "" {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("login must me not empty"),
		}
	}
	return ApiError{
		HTTPStatus: 0,
		Err:        nil,
	}
}

// ProfileHandler for MyApi
func (srv *MyApi) ProfileHandler(w http.ResponseWriter, r *http.Request) {

	params := ProfileParams{}
	finalMap := make(map[string]interface{})
	customErr := params.Fill(r)

	if customErr.Err == nil {

		customErr = params.Validate()
		if customErr.Err != nil {
			finalMap["errApi"] = customErr
		}

		if customErr.Err == nil {
			result, err := srv.Profile(r.Context(), params)
			if err != nil {
				if _, ok := err.(ApiError); ok {
					finalMap["errApi"] = err
				} else {
					finalMap["err"] = err
				}
			} else {
				finalMap["response"] = result
			}
		}
	}

	if customErr.Err != nil {
		finalMap["errApi"] = customErr
	}
	CustomWriteJSON(w, finalMap)
}

// Filling func for CreateParams
func (object *CreateParams) Fill(r *http.Request) ApiError {
	object.Login = r.FormValue("login")
	object.Name = r.FormValue("full_name")
	object.Status = r.FormValue("status")
	age, err := strconv.Atoi(r.FormValue("age"))
	if err != nil {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("age must be int"),
		}
	} else {
		object.Age = age
	}
	return ApiError{
		HTTPStatus: 0,
		Err:        nil,
	}
}

// Validation function for CreateParams
func (object *CreateParams) Validate() ApiError {
	if object.Login == "" {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("login must me not empty"),
		}
	}
	if len(object.Login) < 10 {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("login len must be >= 10"),
		}
	}
	if object.Status == "" {
		object.Status = "user"
	}
	classes := make(map[string]bool)
	classes["user"] = true
	classes["moderator"] = true
	classes["admin"] = true
	if !classes[object.Status] {
		return ApiError{
			HTTPStatus: 400,
			Err:        fmt.Errorf("status must be one of [user, moderator, admin]"),
		}
	}
	if object.Age < 0 {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("age must be >= 0"),
		}
	}
	if object.Age > 128 {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("age must be <= 128"),
		}
	}
	return ApiError{
		HTTPStatus: 0,
		Err:        nil,
	}
}

// CreateHandler for MyApi
func (srv *MyApi) CreateHandler(w http.ResponseWriter, r *http.Request) {

	params := CreateParams{}
	finalMap := make(map[string]interface{})
	customErr := params.Fill(r)

	if customErr.Err == nil {

		customErr = params.Validate()
		if customErr.Err != nil {
			finalMap["errApi"] = customErr
		}

		if customErr.Err == nil {
			result, err := srv.Create(r.Context(), params)
			if err != nil {
				if _, ok := err.(ApiError); ok {
					finalMap["errApi"] = err
				} else {
					finalMap["err"] = err
				}
			} else {
				finalMap["response"] = result
			}
		}
	}

	if customErr.Err != nil {
		finalMap["errApi"] = customErr
	}
	CustomWriteJSON(w, finalMap)
}

// HTTP wrapper for MyApi
func (srv *MyApi) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	finalMap := make(map[string]interface{})
	write := true

	switch r.URL.Path {
	case "/user/profile":
		srv.ProfileHandler(w, r)
		write = false
		break
	case "/user/create":
		if r.Method != "POST" {

			finalMap["errApi"] = ApiError{
				HTTPStatus: 406,
				Err:        fmt.Errorf("bad method"),
			}
			break
		}
		if r.Header.Get("X-Auth") != "100500" {

			finalMap["errApi"] = ApiError{
				HTTPStatus: 403,
				Err:        fmt.Errorf("unauthorized"),
			}
			break
		}
		srv.CreateHandler(w, r)
		write = false
		break

	default:

		finalMap["errApi"] = ApiError{
			HTTPStatus: 404,
			Err:        fmt.Errorf("unknown method"),
		}
	}
	if write {
		CustomWriteJSON(w, finalMap)
	}
}

// Filling func for OtherCreateParams
func (object *OtherCreateParams) Fill(r *http.Request) ApiError {
	object.Username = r.FormValue("username")
	object.Name = r.FormValue("account_name")
	object.Class = r.FormValue("class")
	level, err := strconv.Atoi(r.FormValue("level"))
	if err != nil {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("level must be int"),
		}
	} else {
		object.Level = level
	}
	return ApiError{
		HTTPStatus: 0,
		Err:        nil,
	}
}

// Validation function for OtherCreateParams
func (object *OtherCreateParams) Validate() ApiError {
	if object.Username == "" {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("username must me not empty"),
		}
	}
	if len(object.Username) < 3 {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("username len must be >= 3"),
		}
	}
	if object.Class == "" {
		object.Class = "warrior"
	}
	classes := make(map[string]bool)
	classes["warrior"] = true
	classes["sorcerer"] = true
	classes["rouge"] = true
	if !classes[object.Class] {
		return ApiError{
			HTTPStatus: 400,
			Err:        fmt.Errorf("class must be one of [warrior, sorcerer, rouge]"),
		}
	}
	if object.Level < 1 {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("level must be >= 1"),
		}
	}
	if object.Level > 50 {
		return ApiError{
			HTTPStatus: http.StatusBadRequest,
			Err:        fmt.Errorf("level must be <= 50"),
		}
	}
	return ApiError{
		HTTPStatus: 0,
		Err:        nil,
	}
}

// CreateHandler for OtherApi
func (srv *OtherApi) CreateHandler(w http.ResponseWriter, r *http.Request) {

	params := OtherCreateParams{}
	finalMap := make(map[string]interface{})
	customErr := params.Fill(r)

	if customErr.Err == nil {

		customErr = params.Validate()
		if customErr.Err != nil {
			finalMap["errApi"] = customErr
		}

		if customErr.Err == nil {
			result, err := srv.Create(r.Context(), params)
			if err != nil {
				if _, ok := err.(ApiError); ok {
					finalMap["errApi"] = err
				} else {
					finalMap["err"] = err
				}
			} else {
				finalMap["response"] = result
			}
		}
	}

	if customErr.Err != nil {
		finalMap["errApi"] = customErr
	}
	CustomWriteJSON(w, finalMap)
}

// HTTP wrapper for OtherApi
func (srv *OtherApi) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	finalMap := make(map[string]interface{})
	write := true

	switch r.URL.Path {
	case "/user/create":
		if r.Method != "POST" {

			finalMap["errApi"] = ApiError{
				HTTPStatus: 406,
				Err:        fmt.Errorf("bad method"),
			}
			break
		}
		if r.Header.Get("X-Auth") != "100500" {

			finalMap["errApi"] = ApiError{
				HTTPStatus: 403,
				Err:        fmt.Errorf("unauthorized"),
			}
			break
		}
		srv.CreateHandler(w, r)
		write = false
		break

	default:

		finalMap["errApi"] = ApiError{
			HTTPStatus: 404,
			Err:        fmt.Errorf("unknown method"),
		}
	}
	if write {
		CustomWriteJSON(w, finalMap)
	}
}
