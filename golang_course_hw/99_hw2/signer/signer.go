package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
)

func SingleHash(in, out chan interface{}) {

	var mutex sync.Mutex
	var wg sync.WaitGroup

	for i := range in {

		wg.Add(1)
		result := make(chan string)

		go func(data string, out chan interface{}) {

			go func(data string, result chan string) {
				result <- DataSignerCrc32(data)
				close(result)
			}(data, result) //must be function call

			mutex.Lock()
			md5Result := DataSignerMd5(data) //чтобы не было лока на секунду
			mutex.Unlock()

			crc2Result := DataSignerCrc32(md5Result)
			crc1Result := <-result
			out <- crc1Result + "~" + crc2Result
			wg.Done()
		}(strconv.Itoa(i.(int)), out)
	}
	wg.Wait()
}

func MultiHash(in, out chan interface{}) {

	var wgFunc sync.WaitGroup
	var wgHash sync.WaitGroup

	for i := range in { //для каждой даты создаем функцию и добавляем в waitgroup
		wgFunc.Add(1)

		go func(data string) {

			defer wgFunc.Done()

			output := make([]string, 6, 6)

			for j := 0; j < 6; j++ {
				wgHash.Add(1)
				go func(j int) {
					defer wgHash.Done()
					output[j] = DataSignerCrc32(strconv.Itoa(j) + data)
				}(j)
			}

			wgHash.Wait()
			out <- strings.Join(output, "")

		}(i.(string))

	}
	wgFunc.Wait()
}

func ExecutePipeline(tasks ...job) { // (слайс job)

	wgJobs := &sync.WaitGroup{} // для function call
	chanels := make([]chan interface{}, 1)
	//в work вызываем 2 канала,потому инициализируем channels с емкостью 1 по умолчанию чтобы не было переполнения

	for _ = range tasks {
		chanels = append(chanels, make(chan interface{}, 1))
	}

	for j := range tasks {
		wgJobs.Add(1)

		go func(j int, work []job, wgJobs *sync.WaitGroup, chanels []chan interface{}) {
			defer close(chanels[j+1]) // ждем выполнение jobs[j]
			defer wgJobs.Done()       //уменьшаем счетчик

			work[j](chanels[j], chanels[j+1])

		}(j, tasks, wgJobs, chanels) //выполняем работу chi,ch[i+1]( single и multi)
	}
	wgJobs.Wait()
}

func CombineResults(in, out chan interface{}) {

	var slice []string

	for i := range in {
		val := i.(string)
		slice = append(slice, val)
	}

	sort.Strings(slice)
	out <- strings.Join(slice, "_")

}

func main() {
	in := make(chan interface{})
	out := make(chan interface{})

	go MultiHash(in, out)

	in <- "13314"
	//in <- "13314" - не будет работать в SingleHash так как не используется fmt.

	fmt.Println(<-out)

}
