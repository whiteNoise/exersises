package main

// сюда писать функцию DirTree

import (
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
)

const (
	slashTab = "│\t"
	justTab  = "\t"
	snek     = "└───"
	tBone    = "├───"
)

//for Name and Mode
type path []os.FileInfo

//to implement Sort interface
func (a path) Less(i, j int) bool { return a[i].Name() < a[j].Name() }
func (a path) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a path) Len() int           { return len(a) }

// adds "|	" or "	" to string whether it is node of tree or not
func setDelimers(nodes []bool) string {

	var delimer = ""
	for _, isNode := range nodes {
		if !isNode {
			delimer += justTab
		} else {
			delimer += slashTab
		}
	}
	return delimer
}

//prepares nodes for setDelimers and creates prefix for output
func fillNodes(i int, border int, t []bool, f os.FileInfo) ([]bool, string) {
	var nodes []bool
	result := ""

	if i == border { //if this is not node
		result = setDelimers(t) + snek + f.Name()
		nodes = append(t, false)
	} else {
		result = setDelimers(t) + tBone + f.Name()
		nodes = append(t, true)
	}
	return nodes, result
}


//recursive call for recTree
//printFiles from main checks should we print files or only directories
func dirTree(out io.Writer, pathToFile string, printFiles bool) error {

	var t = []bool{}

	file, err := os.Open(pathToFile) //entry for next iteration
	if err != nil {
		fmt.Println(err)
	}
	return recTree(out, 1, t, file, printFiles)
}

//t []bool{} for setDelimers,also checks depth
func recTree(out io.Writer, level int, t []bool, file *os.File, printFiles bool) error {

	files, err := file.Readdir(0) //list of files in directory

	if err != nil {
		fmt.Println(err)
	}

	if !printFiles {
		var dirs []os.FileInfo
		for _, f := range files {
			if f.Mode().IsDir() { //os.FileInfo.FileMode
				dirs = append(dirs, f)
			}
		}
		files = dirs
	}

	//implemented sort for filenames in dir
	sort.Sort(path(files))

	for i, f := range files {

		var border = len(files) - 1

		nodes, result := fillNodes(i, border, t, f)

		if !f.Mode().IsDir() {
			size := int(f.Size())
			if size == 0 {
				result += " " + "(empty)"
			} else {
				result += " " + "(" + strconv.Itoa(size) + "b" + ")" //(1024b)-size of file
			}
		}

		fmt.Fprintf(out, "%s\n", result)

		if f.Mode().IsDir() {
			nextNode, err := os.Open(file.Name() + "/" + f.Name())
			if err != nil {
				fmt.Println(err)
			}
			error1 := recTree(out, level+1, nodes, nextNode, printFiles)
			if error1 != nil {
				fmt.Println(err)
			}
		}

	}
	return nil
}
