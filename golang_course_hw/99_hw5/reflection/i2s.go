package main

import (
	"fmt"
	"reflect"
)

/*
Первоначальный вариант функции где интерфейс приводился с мапу сразу был кривой,переписал с нуля
*/
func i2s(data interface{}, out interface{}) error {

	setVal, ok := out.(reflect.Value)
	if !ok {
		if reflect.ValueOf(out).Kind() == reflect.Ptr {
			setVal = reflect.ValueOf(out).Elem()
		} else {
			return fmt.Errorf("something strange")
		}
	}
	refData, ok := data.(reflect.Value)
	if !ok {
		refData = reflect.ValueOf(data)
	}
	setFieldData := refData.Kind()

	switch setVal.Kind() {

	case reflect.Slice:
		slice := reflect.MakeSlice(setVal.Type(), 2, 2)
		for i := 0; i < refData.Len(); i++ {
			err := i2s(refData.Index(i).Elem(), slice.Index(i))
			if err != nil {
				return err
			}
		}
		setVal.Set(slice)
	////////////////////////////////////////////////////////
	case reflect.Struct:
		for i := 0; i < setVal.NumField(); i++ {
			name := setVal.Type().Field(i).Name
			valueField := setVal.Field(i)
			setField := setVal.Type().Field(i).Type.Kind()

			switch setFieldData {

			case reflect.Map:
				for _, mapName := range refData.MapKeys() {

					if mapName.String() != name {
						continue
					}

					switch refData.MapIndex(mapName).Elem().Kind() {

					case reflect.Map:
						if setField == reflect.Struct {
							err := i2s(refData.MapIndex(mapName).Elem(), valueField)

							if err != nil {
								return err
							}
							break
						} else {
							return fmt.Errorf("error in simple map")
						}

					case reflect.Slice:
						if setField == reflect.Slice {
							err := i2s(refData.MapIndex(mapName).Elem(), valueField)

							if err != nil {
								return err
							}
							break
						} else {
							return fmt.Errorf("error in simple slice")
						}

					case reflect.Float64:
						if setField == reflect.Int {
							valueField.Set(reflect.ValueOf(int(refData.MapIndex(mapName).Elem().Float())))
							break
						} else {
							return fmt.Errorf("error in simple float")
						}

					case reflect.String:
						if setField == reflect.String {
							valueField.SetString(refData.MapIndex(mapName).Elem().String())
							break
						} else {
							return fmt.Errorf("error in simple string")
						}

					case reflect.Bool:
						if setField == reflect.Bool {
							valueField.SetBool(refData.MapIndex(mapName).Elem().Bool())
							break
						} else {
							return fmt.Errorf("error in simple bool")
						}
					}
				}
			default:
				return fmt.Errorf("mistake in non-slice_struct part")
			}
		}
		////////////////////////////////////////////////
	}
	return nil
}

func main() {

}
