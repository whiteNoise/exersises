package main

import (
<<<<<<< HEAD
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
)


/*
В папках cpuout и memout лежат скрины со сравнениями показателей функций
*/


/*
1)создается мапа с фиксированным размером для избежания аллокаций
2)Используется json-структура для быстрого извлечения нужных полей.Избегаем лишних переводов в map[string]interface
Также не приходится пользоваться regexp
3)используем strings.Replace вместо regexp.MatchStrings
4)Не сохраняем данные которые выводятся на экран в одну строку а сразу выводим
5)Не делаем мапу user-ов
*/
type User struct {
	Browsers []string `json:"browsers"`
	Email    string   `json:"email"`
	Name     string   `json:"name"`
}

func FastSearch(out io.Writer) {

	seenBrowsers := make(map[string]bool, 256) //чтобы не было много аллокаций сразу инициализируем
	var index uint32

	var android bool //переменные для мапы,проверяем уникальность браузера и тип
	var msie bool

	var mail string
	var user User

	file, err := os.OpenFile(filePath, os.O_RDONLY, 0400)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file) //читаем не весь файл сразу

	fmt.Fprintln(out, "found users:")

LOOP:
	for scanner.Scan() {

		err := json.Unmarshal(scanner.Bytes(), &user)
		if err != nil {
			panic(err)
		}

		android = false
		msie = false

		mail = strings.Replace(user.Email, "@", " [at] ", 1) //аналог ReplaceAll

		for _, br := range user.Browsers { //1 цикл вместо двух
			if strings.Contains(br, "Android") {
				android = true
				if !seenBrowsers[br] {
					seenBrowsers[br] = true
				}
			}
			if strings.Contains(br, "MSIE") {
				msie = true
				if !seenBrowsers[br] {
					seenBrowsers[br] = true
				}
			}
		}

		index++
		if !(android && msie) {
			continue LOOP
		}

		fmt.Fprintf(out, "[%d] %s <%s>\n", index-1, user.Name, mail) //не делаем одну огромную строку,сразу выводим
	}

	fmt.Fprintf(out, "\nTotal unique browsers %d\n", len(seenBrowsers))
}

func main() {

}
=======
	"io"
)

// вам надо написать более быструю оптимальную этой функции
func FastSearch(out io.Writer) {
	SlowSearch(out)
}
>>>>>>> 16cc71203057178da3332275c450825aa3d0ce90
