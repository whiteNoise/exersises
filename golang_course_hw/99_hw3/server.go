package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type SearchClient struct {
	FindUsers func() string
} //пока пофиг на это

func SearchServer(w http.ResponseWriter, r *http.Request) { //роутер
	r.ParseForm()       //анализ аргументов,
	fmt.Println(r.Form) // ввод информации о форме на стороне сервера
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		fmt.Println("key:", k)
		fmt.Println("val:", strings.Join(v, ""))
	}
	fmt.Fprintf(w, "Hello Maksim!") // отправляем данные на клиентскую сторону
} //и на это пофиг пока

type Rows struct {
	XMLName xml.Name `xml:"root"`
	Columns []Row    `xml:"row"`
}

type Row struct {
	XMLName   xml.Name `xml:"row"`
	ID        string   `xml:"id"`
	Age       string   `xm:"age"`
	About     string   `xml:"about"`
	FirstName string   `xml:"first_name"`
	LastName  string   `xml:"last_name"`
}

/*func ParseXML(s string) {

	xmlFile, err := os.Open(s) // открываем файл

	if err != nil {
		fmt.Println(err)
	}

	defer xmlFile.Close() // закрываем чтобы можно было парсить

	byteValue, _ := ioutil.ReadAll(xmlFile) // преобразуем XML в массив байт

	var rows Rows

	xml.Unmarshal(byteValue, &rows)

	for i := 0; i < len(rows.Columns); i++ {
		fmt.Println("User id: " + rows.Columns[i].ID)
		fmt.Println("User Name: " + rows.Columns[i].FirstName + " " + rows.Columns[i].LastName)
		fmt.Println("User age: " + rows.Columns[i].Age)
		fmt.Println("About: " + rows.Columns[i].About)
	}
}
*/
func main() {
	xmlFile, err := os.Open("/Users/aleksandrilin/Desktop/go/3/99_hw/coverage/dataset.xml") // открываем файл

	if err != nil {
		fmt.Println(err)
	}

	defer xmlFile.Close() // закрываем чтобы можно было парсить

	byteValue, _ := ioutil.ReadAll(xmlFile) // преобразуем XML в массив байт

	var rows Rows

	xml.Unmarshal(byteValue, &rows)

	for i := 0; i < len(rows.Columns); i++ {
		fmt.Println("User id: " + rows.Columns[i].ID)
		fmt.Println("User Name: " + rows.Columns[i].FirstName + " " + rows.Columns[i].LastName)
		fmt.Println("User age: " + rows.Columns[i].Age)
		fmt.Println("About: " + rows.Columns[i].About)
	}
}
