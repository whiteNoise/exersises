package main

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

const OkToken = "1066480043"

type Clients struct {
	Rows []xmlData `xml:"row"`
}

type xmlData struct {
	Id        int    `xml:"id"`
	Age       int    `xml:"age"`
	FirstName string `xml:"first_name"`
	LastName  string `xml:"last_name"`
	Gender    string `xml:"gender"`
	About     string `xml:"about"`
}

func (data xmlData) convert() User {
	return User{
		Id:     data.Id,
		Name:   data.FirstName + " " + data.LastName,
		Age:    data.Age,
		About:  data.About,
		Gender: data.Gender,
	}
}

func SortBy(field string, order string, users []User) ([]User, error) {
	if field == "Age" {
		switch order {
		case "1":
			sort.Slice(users, func(i, j int) bool {
				return users[i].Age < users[j].Age
			})
			return users, nil
		case "-1":
			sort.Slice(users, func(i, j int) bool {
				return users[i].Age > users[j].Age
			})
			return users, nil
		case "0":
			return users, nil
		}
	}
	if field == "Id" {
		switch order {
		case "1":
			sort.Slice(users, func(i, j int) bool {
				return users[i].Id < users[j].Id
			})
			return users, nil
		case "-1":
			sort.Slice(users, func(i, j int) bool {
				return users[i].Id > users[j].Id
			})
			return users, nil
		case "0":
			return users, nil
		}

	}
	if field == "" || field == "Name" {
		switch order {
		case "1":
			sort.Slice(users, func(i, j int) bool {
				return users[i].Name < users[j].Name
			})
			return users, nil
		case "-1":
			sort.Slice(users, func(i, j int) bool {
				return users[i].Name > users[j].Name
			})
			return users, nil
		case "0":
			return users, nil
		}

	}
	return users, errors.New("ErrorBaxdOrderField")
}

func SearchServer(w http.ResponseWriter, r *http.Request) {

	token := r.Header.Get("AccessToken")
	if token == OkToken {
		clientSlice := &Clients{}
		data, _ := ioutil.ReadFile("dataset.xml")

		err := xml.Unmarshal(data, &clientSlice)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		UsersData := []User{}
		for _, val := range clientSlice.Rows {
			UsersData = append(UsersData, val.convert())
		}

		orderField := r.FormValue("order_field")
		orderBy := r.FormValue("order_by")
		offset, _ := strconv.Atoi(r.FormValue("offset"))
		limit, _ := strconv.Atoi(r.FormValue("limit"))
		query := r.FormValue("query")

		var users []User

	Point:
		for {
			switch orderBy {
			case "1":
				break Point
			case "-1":
				break Point
			case "0":
				break Point
			default:
				w.WriteHeader(http.StatusBadRequest)
				error := []byte(`{"Error":"ERROR_OF_ORDERBY"}`)
				w.Write(error)
				return
			}
		}

		for _, user := range UsersData {
			if strings.Contains(user.Name, query) || strings.Contains(user.About, query) {
				users = append(users, user)
			}
		}

		users, err2 := SortBy(orderField, orderBy, users)
		if err2 != nil {
			w.WriteHeader(http.StatusBadRequest)
			error := []byte(`{"Error":"ErrorBadOrderField"}`)
			w.Write(error)
			return
		}

		if offset+limit > len(UsersData) {
			users = users[offset:]
		} else {
			users = users[offset : offset+limit]
		}

		result, _ := json.Marshal(users)
		w.WriteHeader(http.StatusOK)
		w.Write(result)

	} else {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
}

func main() {

}
