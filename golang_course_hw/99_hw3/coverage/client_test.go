package main

//тут писать код тестов

import (
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
	"time"
)

type Test struct {
	Input         *SearchRequest
	ClientSetting *SearchClient
	Result        *SearchResponse
	Error         string
}

func SearchServerTimeOut(w http.ResponseWriter, r *http.Request) {
	time.Sleep(2 * time.Second)
}
func SearchServerBadJson(w http.ResponseWriter, r *http.Request) {
	var jsonAns []byte
	w.WriteHeader(http.StatusOK)
	w.Write(jsonAns)
}
func SearchServerJsonBadRequest(w http.ResponseWriter, r *http.Request) {
	var jsonAns []byte
	w.WriteHeader(http.StatusBadRequest)
	w.Write(jsonAns)
}

func TestСant(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      2,
				Offset:     34,
				OrderField: "Age",
				OrderBy:    -1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     15,
						Name:   "Allison Valdez",
						Age:    21,
						Gender: "male",
						About:  "Labore excepteur voluptate velit occaecat est nisi minim. Laborum ea et irure nostrud enim sit incididunt reprehenderit id est nostrud eu. Ullamco sint nisi voluptate cillum nostrud aliquip et minim. Enim duis esse do aute qui officia ipsum ut occaecat deserunt. Pariatur pariatur nisi do ad dolore reprehenderit et et enim esse dolor qui. Excepteur ullamco adipisicing qui adipisicing tempor minim aliquip.\n",
					},
				},
				NextPage: false,
			},
			Error: "",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err != nil {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
		if !reflect.DeepEqual(item.Result, res) {
			t.Errorf("[%d] wrong result, expected:\n %#v,\n\n GOT: \n%#v", caseNum, item.Result, res)
		}
	}
	handle.Close()
}
func TestSortByAge(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      1,
				Offset:     0,
				OrderField: "Age",
				OrderBy:    -1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     32,
						Name:   "Christy Knapp",
						Age:    40,
						Gender: "female",
						About:  "Incididunt culpa dolore laborum cupidatat consequat. Aliquip cupidatat pariatur sit consectetur laboris labore anim labore. Est sint ut ipsum dolor ipsum nisi tempor in tempor aliqua. Aliquip labore cillum est consequat anim officia non reprehenderit ex duis elit. Amet aliqua eu ad velit incididunt ad ut magna. Culpa dolore qui anim consequat commodo aute.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
		Test{
			Input: &SearchRequest{
				Limit:      1,
				Offset:     2,
				OrderField: "Age",
				OrderBy:    0,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     2,
						Name:   "Brooks Aguilar",
						Age:    25,
						Gender: "male",
						About:  "Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
		Test{
			Input: &SearchRequest{
				Limit:      1,
				Offset:     4,
				OrderField: "Age",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     14,
						Name:   "Nicholson Newman",
						Age:    23,
						Gender: "male",
						About:  "Tempor minim reprehenderit dolore et ad. Irure id fugiat incididunt do amet veniam ex consequat. Quis ad ipsum excepteur eiusmod mollit nulla amet velit quis duis ut irure.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err != nil {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
		if !reflect.DeepEqual(item.Result, res) {
			t.Errorf("[%d] wrong result, expected:\n %#v,\n\n GOT: \n%#v", caseNum, item.Result, res)
		}
	}
	handle.Close()
}
func TestSortByName(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      1,
				Offset:     2,
				OrderField: "Name",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     19,
						Name:   "Bell Bauer",
						Age:    26,
						Gender: "male",
						About:  "Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
		Test{
			Input: &SearchRequest{
				Limit:      1,
				Offset:     2,
				OrderField: "Name",
				OrderBy:    0,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     2,
						Name:   "Brooks Aguilar",
						Age:    25,
						Gender: "male",
						About:  "Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
		Test{
			Input: &SearchRequest{
				Limit:      1,
				Offset:     0,
				OrderField: "Name",
				OrderBy:    -1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     13,
						Name:   "Whitley Davidson",
						Age:    40,
						Gender: "male",
						About:  "Consectetur dolore anim veniam aliqua deserunt officia eu. Et ullamco commodo ad officia duis ex incididunt proident consequat nostrud proident quis tempor. Sunt magna ad excepteur eu sint aliqua eiusmod deserunt proident. Do labore est dolore voluptate ullamco est dolore excepteur magna duis quis. Quis laborum deserunt ipsum velit occaecat est laborum enim aute. Officia dolore sit voluptate quis mollit veniam. Laborum nisi ullamco nisi sit nulla cillum et id nisi.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err != nil {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
		if !reflect.DeepEqual(item.Result, res) {
			t.Errorf("[%d] wrong result, expected:\n %#v,\n\n GOT: \n%#v", caseNum, item.Result, res)
		}
	}
	handle.Close()
}
func TestOrderByErr(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      0,
				Offset:     0,
				OrderField: "Name",
				OrderBy:    4,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: nil,
			Error:  "unknown bad request error: ERROR_OF_ORDERBY",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	handle.Close()
}
func TestOrderBadJson(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      0,
				Offset:     0,
				OrderField: "Name",
				OrderBy:    4,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: nil,
			Error:  "cant unpack result json: unexpected end of JSON input",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServerBadJson))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	handle.Close()
}
func TestOrderJsonBadRequest(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      3,
				Offset:     0,
				OrderField: "Name",
				OrderBy:    4,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: nil,
			Error:  "cant unpack error json: unexpected end of JSON input",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServerJsonBadRequest))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	handle.Close()
}

func TestXMLError(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      0,
				Offset:     0,
				OrderField: "Name",
				OrderBy:    4,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: nil,
			Error:  "SearchServer fatal error",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {

		item.ClientSetting.URL = handle.URL
		emptyFile, _ := os.Create("empty.xml")
		emptyFile.Close()

		oldName := "dataset.xml"
		newName := "test.xml"
		_ = os.Rename(oldName, newName)
		oldName = "empty.xml"
		newName = "dataset.xml"
		_ = os.Rename(oldName, newName)

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
		_ = os.Remove("dataset.xml")
		oldName = "test.xml"
		newName = "dataset.xml"
		_ = os.Rename(oldName, newName)
	}
	handle.Close()
}
func TestServerError(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      148,
				Offset:     0,
				OrderField: "REDRUM",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: nil,
			Error:  "OrderFeld REDRUM invalid",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	handle.Close()
}

func TestSortID(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      3,
				Offset:     3,
				OrderField: "Id",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     3,
						Name:   "Everett Dillard",
						Age:    27,
						Gender: "male",
						About:  "Sint eu id sint irure officia amet cillum. Amet consectetur enim mollit culpa laborum ipsum adipisicing est laboris. Adipisicing fugiat esse dolore aliquip quis laborum aliquip dolore. Pariatur do elit eu nostrud occaecat.\n",
					},
					User{
						Id:     4,
						Name:   "Owen Lynn",
						Age:    30,
						Gender: "male",
						About:  "Elit anim elit eu et deserunt veniam laborum commodo irure nisi ut labore reprehenderit fugiat. Ipsum adipisicing labore ullamco occaecat ut. Ea deserunt ad dolor eiusmod aute non enim adipisicing sit ullamco est ullamco. Elit in proident pariatur elit ullamco quis. Exercitation amet nisi fugiat voluptate esse sit et consequat sit pariatur labore et.\n",
					},
					User{
						Id:     5,
						Name:   "Beulah Stark",
						Age:    30,
						Gender: "female",
						About:  "Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
		Test{
			Input: &SearchRequest{
				Limit:      1,
				Offset:     3,
				OrderField: "Id",
				OrderBy:    0,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     3,
						Name:   "Everett Dillard",
						Age:    27,
						Gender: "male",
						About:  "Sint eu id sint irure officia amet cillum. Amet consectetur enim mollit culpa laborum ipsum adipisicing est laboris. Adipisicing fugiat esse dolore aliquip quis laborum aliquip dolore. Pariatur do elit eu nostrud occaecat.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
		Test{
			Input: &SearchRequest{
				Limit:      3,
				Offset:     29,
				OrderField: "Id",
				OrderBy:    -1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},
			Result: &SearchResponse{
				Users: []User{
					User{
						Id:     5,
						Name:   "Beulah Stark",
						Age:    30,
						Gender: "female",
						About:  "Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.\n",
					},
					User{
						Id:     4,
						Name:   "Owen Lynn",
						Age:    30,
						Gender: "male",
						About:  "Elit anim elit eu et deserunt veniam laborum commodo irure nisi ut labore reprehenderit fugiat. Ipsum adipisicing labore ullamco occaecat ut. Ea deserunt ad dolor eiusmod aute non enim adipisicing sit ullamco est ullamco. Elit in proident pariatur elit ullamco quis. Exercitation amet nisi fugiat voluptate esse sit et consequat sit pariatur labore et.\n",
					},
					User{
						Id:     3,
						Name:   "Everett Dillard",
						Age:    27,
						Gender: "male",
						About:  "Sint eu id sint irure officia amet cillum. Amet consectetur enim mollit culpa laborum ipsum adipisicing est laboris. Adipisicing fugiat esse dolore aliquip quis laborum aliquip dolore. Pariatur do elit eu nostrud occaecat.\n",
					},
				},
				NextPage: true,
			},
			Error: "",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err != nil {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
		if !reflect.DeepEqual(item.Result, res) {
			t.Errorf("[%d] wrong result, expected:\n %#v,\n\n GOT: \n%#v", caseNum, item.Result, res)
		}
	}
	handle.Close()
}

func TestBadLimitAndOfset(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      -11313,
				Offset:     3,
				OrderField: "Id",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},

			Error: "limit must be > 0",
		},
		Test{
			Input: &SearchRequest{
				Limit:      2,
				Offset:     -1031,
				Query:      "fsdfsgdsdgsdg",
				OrderField: "Id",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},

			Error: "offset must be > 0",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	handle.Close()
}

func TestBadToken(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      114,
				Offset:     0,
				OrderField: "Name",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "YOUSHALLNOTPASS",
			},
			Result: nil,
			Error:  "Bad AccessToken",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServer))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	handle.Close()
}

func TestTimeout(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      25,
				Offset:     27,
				OrderField: "Id",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "1066480043",
			},

			Error: "timeout for limit=26&offset=27&order_by=1&order_field=Id&query=",
		},
	}

	handle := httptest.NewServer(http.HandlerFunc(SearchServerTimeOut))

	for caseNum, item := range cases {
		item.ClientSetting.URL = handle.URL

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
		if err.Error() != item.Error {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	handle.Close()
}

func TestUnknown(t *testing.T) {
	cases := []Test{
		Test{
			Input: &SearchRequest{
				Limit:      228,
				Offset:     0,
				OrderField: "Name",
				OrderBy:    1,
			},
			ClientSetting: &SearchClient{
				AccessToken: "OLOLINNI",
				URL:         "",
			},
			Result: nil,
			Error:  "unknown error",
		},
	}

	for caseNum, item := range cases {

		res, err := item.ClientSetting.FindUsers((*item.Input))

		if err == nil {
			t.Errorf("[%d] must be error: %#v", caseNum, res)
		}
	}

}
