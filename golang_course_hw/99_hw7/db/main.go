// тут лежит тестовый код
// менять вам может потребоваться только коннект к базе
package main

//7.1 03538 042

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var (
	// DSN это соединение с базой
	// вы можете изменить этот на тот который вам нужен
	// docker run -p 4000:4000 -v $(PWD):/docker-entrypoint-initdb.d -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_DATABASE=golang -d mysql
	DSN = "root:1234@tcp(localhost:4000)/golang?charset=utf8"
	// DSN = "coursera:5QPbAUufx7@tcp(localhost:3306)/coursera?charset=utf8"
)

func main() {

	//документ с базами данных
	db, err := sql.Open("mysql", DSN)

	if err != nil {
		fmt.Println("sql open error", err)
	}
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(0)
	db.SetConnMaxLifetime(time.Second * 10)

	err = db.Ping() // вот тут будет первое подключение к базе
	if err != nil {
		fmt.Println("connection error", err)
	}

	//в NewDbExplorer нужно хендлить все функции пользователя
	handler, err := NewDbExplorer(db) //по сути handler это mux.NewRouter из гориллы
	if err != nil {
		panic(err)
	}

	fmt.Println("starting server at :8082")
	http.ListenAndServe(":8082", handler)
}
