package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

type Response struct {
	Resp  interface{} `json:"response,omitempty"`
	Error string      `json:"error,omitempty"`
}

type ResponceElem struct {
	IsNull   bool
	PrimeKey bool
	Name     string
	Type     string
}

type SQLManager struct {
	Tables      []string
	DB          *sql.DB
	TableMap    map[string][]*ResponceElem
	FieldsMap   map[string]map[string]int
	OneRecord   *regexp.Regexp
	Standart    *regexp.Regexp
	CustomInput *regexp.Regexp
}

// Route
func (h *SQLManager) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	switch r.Method {

	case http.MethodDelete:
		h.customDelete(w, r)

	case http.MethodPost:
		h.customPost(w, r)

	case http.MethodPut:
		h.customPut(w, r)

	case http.MethodGet:
		if r.URL.Path == "/" {
			h.getTables(w, r)
			return
		} else {
			h.customGet(w, r)
		}

	default:
		w.WriteHeader(http.StatusNotFound)
		return
	}
}

// init manager
func NewDbExplorer(db *sql.DB) (*SQLManager, error) {

	Explorer := &SQLManager{

		DB:          db,
		TableMap:    make(map[string][]*ResponceElem),
		FieldsMap:   make(map[string]map[string]int),
		OneRecord:   regexp.MustCompile(`(?m)\/(\w+)\/(\d+)$`),
		Standart:    regexp.MustCompile(`(?m)\/(\w+)$`),
		CustomInput: regexp.MustCompile(`(?m)\/(\w+)\/$`),
	}

	// table list
	rows, err := Explorer.DB.Query("SHOW TABLES")
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var table string
		err = rows.Scan(&table)
		if err != nil {
			return nil, err
		}
		Explorer.Tables = append(Explorer.Tables, table)
		Explorer.TableMap[table] = make([]*ResponceElem, 0)
		Explorer.FieldsMap[table] = make(map[string]int, 0)
	}
	rows.Close()

	// info about fields
	for _, table := range Explorer.Tables {

		query := "SHOW FULL COLUMNS FROM `" + table + "`"
		rows, err := db.Query(query)
		if err != nil {
			return nil, err
		}

		for rows.Next() {
			columns, err := rows.Columns()
			if err != nil {
				return nil, err
			}

			vals := make([]interface{}, len(columns))
			for i, _ := range columns {
				vals[i] = new(sql.RawBytes)
			}

			err = rows.Scan(vals...)
			if err != nil {
				return nil, err
			}

			newVals := make([]string, len(columns))
			for i, val := range vals {
				bytes, _ := val.(*sql.RawBytes)
				newVals[i] = string([]byte(*bytes))
			}

			var canNULL bool
			if newVals[3] == `YES` {
				canNULL = true
			}

			typeField := "int"
			if newVals[1] == `text` || strings.Contains(newVals[1], `varchar`) {
				typeField = `string`
			}

			var isKey bool
			if newVals[6] == `auto_increment` {
				isKey = true
			}

			//create field
			field := &ResponceElem{
				Name:     newVals[0],
				Type:     typeField,
				IsNull:   canNULL,
				PrimeKey: isKey,
			}

			//set field
			Explorer.FieldsMap[table][field.Name] = len(Explorer.FieldsMap[table])
			Explorer.TableMap[table] = append(Explorer.TableMap[table], field)
		}
		rows.Close()
	}

	return Explorer, nil
}

// table list
func (h *SQLManager) getTables(w http.ResponseWriter, r *http.Request) {

	customResponse := &Response{}
	customResponse.Resp = map[string]interface{}{
		"tables": h.Tables,
	}
	result, _ := json.Marshal(customResponse)
	w.Write(result)
}

// DELETE
func (h *SQLManager) customDelete(w http.ResponseWriter, r *http.Request) {

	response := &Response{}

	customResult := h.OneRecord.FindStringSubmatch(r.URL.Path)

	if len(customResult) != 3 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	table := customResult[1]
	if _, ok := h.TableMap[table]; !ok {

		w.WriteHeader(http.StatusNotFound)
		response.Error = "unknown table"
		res, _ := json.Marshal(response)
		w.Write(res)
		return
	}

	id, err := strconv.Atoi(customResult[2])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	res, err := h.deleteRecord(table, id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response.Error = err.Error()
		res, _ := json.Marshal(response)
		w.Write(res)
		return
	}

	response.Resp = map[string]interface{}{
		"deleted": res,
	}
	result, _ := json.Marshal(response)
	w.Write(result)

}

// PUT
func (h *SQLManager) customPut(w http.ResponseWriter, r *http.Request) {

	response := &Response{}

	customResult := h.CustomInput.FindStringSubmatch(r.URL.Path)

	if len(customResult) != 2 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	table := customResult[1]
	if _, ok := h.TableMap[table]; !ok {
		w.WriteHeader(http.StatusNotFound)
		response.Error = "unknown table"
		res, _ := json.Marshal(response)
		w.Write(res)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	mapData, _ := data.(map[string]interface{})

	res, err := h.createRecord(table, mapData)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	response.Resp = res
	answer, _ := json.Marshal(response)
	w.Write(answer)
}

// POST
func (h *SQLManager) customPost(w http.ResponseWriter, r *http.Request) {

	response := &Response{}

	customResult := h.OneRecord.FindStringSubmatch(r.URL.Path)
	if len(customResult) != 3 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	table := customResult[1]

	if _, ok := h.TableMap[table]; !ok {
		w.WriteHeader(http.StatusNotFound)
		response.Error = "unknown table"
		res, _ := json.Marshal(response)
		w.Write(res)
		return
	}

	id, err := strconv.Atoi(customResult[2])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)

	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	mapData, _ := data.(map[string]interface{})

	res, err := h.updateRecord(table, id, mapData)

	if res == nil {
		response.Error = "record not found"
		w.WriteHeader(http.StatusNotFound)
		answer, _ := json.Marshal(response)
		w.Write(answer)

	}

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response.Error = err.Error()
		result, _ := json.Marshal(response)
		w.Write(result)
		return
	}

	if res != nil {
		response.Resp = map[string]interface{}{
			"updated": res,
		}
		answer, _ := json.Marshal(response)
		w.Write(answer)
	}

}

// GET
func (h *SQLManager) customGet(w http.ResponseWriter, r *http.Request) {
	response := &Response{}

	// /$table/$id
	customResult := h.OneRecord.FindStringSubmatch(r.URL.Path)

	if len(customResult) == 3 {

		table := customResult[1]
		if _, ok := h.TableMap[table]; !ok {
			w.WriteHeader(http.StatusNotFound)
			response.Error = "unknown table"
			res, _ := json.Marshal(response)
			w.Write(res)
			return

		}

		id, err := strconv.Atoi(customResult[2])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		res, err := h.getRecord(table, id)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if res == nil {
			response.Error = "record not found"
			w.WriteHeader(http.StatusNotFound)
			answer, _ := json.Marshal(response)
			w.Write(answer)
		} else {
			response.Resp = map[string]interface{}{
				"record": res,
			}
			answer, _ := json.Marshal(response)
			w.Write(answer)
		}

	} else {

		// /$table
		customResult = h.Standart.FindStringSubmatch(r.URL.Path)
		lenght := len(customResult)

		if lenght != 2 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var limit int
		var offset int
		var err error

		table := customResult[1]

		if _, ok := h.TableMap[table]; !ok {
			w.WriteHeader(http.StatusNotFound)
			response.Error = "unknown table"
			res, _ := json.Marshal(response)
			w.Write(res)
			return
		}

		tmp := r.FormValue("limit")
		if tmp == "" {
			limit = 5
		} else {
			limit, err = strconv.Atoi(tmp)
			if err != nil {
				limit = 5
			}
		}

		tmp = r.FormValue("offset")
		if tmp == "" {
			offset = 0
		} else {
			offset, err = strconv.Atoi(tmp)
			if err != nil {
				offset = 0
			}
		}

		res, err := h.getMultipleRecords(table, limit, offset)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		response.Resp = map[string]interface{}{
			"records": res,
		}
		answer, _ := json.Marshal(response)
		w.Write(answer)

	}
}

// fill with default values
func (h *SQLManager) fillDefault(vals []interface{}, table string, columns []string) []interface{} {

	newVals := make([]interface{}, len(columns))

	for i, val := range vals {
		if val != nil {
			bytes, _ := val.(*sql.RawBytes)

			if len(*bytes) == 0 {
				if h.TableMap[table][i].IsNull {
					newVals[i] = nil
				} else {
					if h.TableMap[table][i].Type == `string` {
						newVals[i] = ""
					}
					if h.TableMap[table][i].Type == `int` {
						newVals[i] = 0
					}
				}

			} else {
				newVals[i] = string([]byte(*bytes))
				if h.TableMap[table][i].Type == `int` {
					tmp, _ := strconv.Atoi(newVals[i].(string))
					newVals[i] = tmp
				}
			}
		} else {
			newVals[i] = nil
		}
	}
	return newVals
}

// get single
func (h *SQLManager) getRecord(table string, id int) (interface{}, error) {

	record := make(map[string]interface{})

	query := "SELECT * FROM `" + table + "` WHERE `" + h.TableMap[table][0].Name + "`=?"

	rows, err := h.DB.Query(query, id)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	var found bool

	for rows.Next() {

		found = true
		columns, err := rows.Columns()
		if err != nil {
			return nil, err
		}

		vals := make([]interface{}, len(columns))

		for i := 0; i < len(columns); i++ {
			vals[i] = new(sql.RawBytes)
		}

		err = rows.Scan(vals...)
		if err != nil {
			return nil, err
		}

		//fill with default
		newVals := h.fillDefault(vals, table, columns)

		for i, val := range newVals {
			record[columns[i]] = val
		}

	}
	if found {
		return record, nil
	} else {
		return nil, nil
	}

}

// get multiple
func (h *SQLManager) getMultipleRecords(table string, limit int, offset int) (interface{}, error) {

	res := []map[string]interface{}{}

	query := "SELECT * FROM " + table + " LIMIT ? OFFSET ?"
	rows, err := h.DB.Query(query, limit, offset)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		curRecord := make(map[string]interface{})

		columns, err := rows.Columns()
		if err != nil {
			return nil, err
		}

		vals := make([]interface{}, len(columns))
		for i, _ := range columns {
			vals[i] = new(sql.RawBytes)
		}

		err = rows.Scan(vals...)
		if err != nil {
			return nil, err
		}

		//fill with default
		newVals := h.fillDefault(vals, table, columns)

		//create record
		for i, val := range newVals {
			curRecord[columns[i]] = val
		}

		res = append(res, curRecord)
	}

	rows.Close()
	return res, nil
}

// C
func (h *SQLManager) createRecord(table string, data map[string]interface{}) (interface{}, error) {

	var items string
	var delimers string
	values := []interface{}{}

	var primaryKey string
	for _, val := range h.TableMap[table] {
		if !val.PrimeKey {
			items += val.Name + ", "
			if field, ok := data[val.Name]; ok {
				delimers += "?, "
				values = append(values, field)
			} else {
				delimers += "'', "
			}
		} else {
			primaryKey = val.Name
		}
	}

	delimers = delimers[:len(delimers)-2]
	items = items[:len(items)-2]

	query := fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)", table, items, delimers)

	res, err := h.DB.Exec(query, values...)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	answer := map[string]interface{}{
		primaryKey: id,
	}
	return answer, nil
}

// U
func (h *SQLManager) updateRecord(table string, id int, data map[string]interface{}) (interface{}, error) {

	var primaryKey string

	for _, val := range h.TableMap[table] {
		if val.PrimeKey {
			primaryKey = val.Name
			break
		}
	}

	var items string
	values := []interface{}{}
	for key, val := range data {

		if idx, ok := h.FieldsMap[table][key]; ok {

			if !h.TableMap[table][idx].PrimeKey {

				if val == nil && !h.TableMap[table][idx].IsNull {
					return nil, fmt.Errorf("field %s have invalid type", key)
				}
				switch val.(type) {
				case float64:
					if h.TableMap[table][idx].Type != `int` {
						return nil, fmt.Errorf("field %s have invalid type", key)
					}
				case string:
					if h.TableMap[table][idx].Type != `string` {
						return nil, fmt.Errorf("field %s have invalid type", key)
					}
				}
				items += key + " = ?, "
				values = append(values, val)
			} else {
				return nil, fmt.Errorf("field %s have invalid type", key)
			}
		}
	}
	items = items[:len(items)-2]

	query := fmt.Sprintf("UPDATE %s SET %s WHERE %s = ?", table, items, primaryKey)

	values = append(values, id)
	res, err := h.DB.Exec(query, values...)
	if err != nil {
		return nil, err
	}
	updated, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}
	return updated, nil
}

// D
func (h *SQLManager) deleteRecord(table string, id int) (interface{}, error) {

	var primaryKey string

	for _, val := range h.TableMap[table] {
		if val.PrimeKey {
			primaryKey = val.Name
			break
		}
	}

	query := fmt.Sprintf("DELETE FROM %s WHERE %s = ?", table, primaryKey)

	res, err := h.DB.Exec(query, id)
	if err != nil {
		return nil, err
	}
	updated, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}
	return updated, nil
}
