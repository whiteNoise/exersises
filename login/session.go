package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/satori/go.uuid"
	"github.com/xeipuuv/gojsonschema"
)

func getUser(w http.ResponseWriter, r *http.Request) user {
	c, err := r.Cookie("session")
	if err != nil {
		sessionID, _ := uuid.NewV4()
		c = &http.Cookie{
			Name:  "session",
			Value: sessionID.String(),
		}
	}
	c.MaxAge = sessionLength
	http.SetCookie(w, c)
	var u user
	if ses, ok := dbSsessions[c.Value]; ok {
		u = dbUsers[ses.login]
	}
	return u
}

func alreadyLoggedIn(r *http.Request) bool {
	c, err := r.Cookie("session")
	if err != nil {
		return false
	}
	ses := dbSsessions[c.Value]
	_, ok := dbUsers[ses.login]
	return ok
}

func cleanSessions() {
	for k, v := range dbSsessions {
		if time.Now().Sub(v.time) > (time.Second * 3600) {
			delete(dbSsessions, k)
		}
	}
}
func checkJson(w http.ResponseWriter, source []byte) {

	schema, err := gojsonschema.NewSchema(schemaLoader)
	if err != nil {
		SendError(w, http.StatusInternalServerError, err)
		return
	}
	schemaLoader := gojsonschema.NewBytesLoader(source)
	result, err := schema.Validate(schemaLoader)
	if err != nil {
		SendError(w, http.StatusInternalServerError, err)
		return
	}
	if !result.Valid() {
		SendError(w, http.StatusBadRequest, fmt.Errorf("Bad Schema"))
		return
	}
}
func SendError(w http.ResponseWriter, status int, err error) {
	w.WriteHeader(status)
	w.Header().Set("Content-type", "application/json")
	jsonError, _ := json.Marshal(err.Error())
	w.Write(jsonError)
}
