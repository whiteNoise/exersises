package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"github.com/xeipuuv/gojsonschema"
	"golang.org/x/crypto/bcrypt"
)

/*TODO:
create sessions and users database
run API only from signed and logged users
create normal middleware
*/
type user struct {
	UUID     string `json: "uuid"`
	Login    string
	Password []byte
	Tz       []string `json: "uuid"`
}
type session struct {
	login string
	name  string
	time  time.Time
}

type Handler struct {
	DB    *sql.DB
	Users []*user
}

var dbUsers = map[string]user{}        //uuid/user
var dbSsessions = map[string]session{} //id/session
var currTime time.Time
var (
	schema = `{
	"type": "object",
	"properties": {
		"UUID": {
			"type": "string"
		},
		"Tz": {
			"type": "array",
			"items": {"type": "string"}
		}
	}
}`
	schemaLoader = gojsonschema.NewStringLoader(schema)
	mu           = &sync.Mutex{}
)

const sessionLength int = 3600

func main() {
	db, err := sql.Open("mysql", "root:1234@tcp(127.0.0.1:3306)/timezones")
	if err != nil {
		log.Fatalln("error creating db: ", err)
	}
	db.SetMaxOpenConns(100)
	db.SetConnMaxLifetime(time.Second)

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	defer db.Close()
	handler := &Handler{
		DB: db,
	}
	router := mux.NewRouter()
	router.HandleFunc("/signup", handler.signUp)
	router.HandleFunc("/login", handler.login)
	router.HandleFunc("/tz/{uuid}", handler.GetTZFromSingleUser).Methods("GET")
	router.HandleFunc("/tz/new", handler.CreateTZ).Methods("POST")
	router.HandleFunc("/tz/{uuid}", handler.UpdateTZ).Methods("PUT")
	router.HandleFunc("/tz/{uuid}", handler.DeleteTZ).Methods("DELETE")
	http.ListenAndServe("localhost:8080", router)
}

func (h *Handler) login(w http.ResponseWriter, r *http.Request) {

	if alreadyLoggedIn(r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	if r.Method == http.MethodPost {
		login := r.FormValue("login")
		pwd := r.FormValue("password")
		u, ok := dbUsers[login]
		if !ok {
			http.Error(w, "Username or password do not match", http.StatusForbidden)
			return
		}
		err := bcrypt.CompareHashAndPassword(u.Password, []byte(pwd))
		if err != nil {
			http.Error(w, "Username or password do not match", http.StatusForbidden)
			return
		}

		sessionID, _ := uuid.NewV4()
		c := &http.Cookie{
			Name:  "session",
			Value: sessionID.String(),
		}
		http.SetCookie(w, c)
		dbSsessions[c.Value] = session{
			login: login,
			time:  time.Now(),
		}
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
}

func (h *Handler) signUp(w http.ResponseWriter, r *http.Request) {
	if alreadyLoggedIn(r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	if r.Method == http.MethodPost {
		login := r.FormValue("login")
		password := r.FormValue("password")
		name := r.FormValue("name")

		if _, ok := dbUsers[login]; ok {
			http.Error(w, "Username already exists", http.StatusForbidden)
			return
		}
		if len(login) < 7 || len(password) < 7 || len(name) < 7 {
			http.Error(w, "incorrect uder data", http.StatusForbidden)
			return
		}
		sessionID, _ := uuid.NewV4()
		userID, _ := uuid.NewV2(uuid.DomainPerson)
		c := &http.Cookie{
			Name:  "session",
			Value: sessionID.String(),
		}
		http.SetCookie(w, c)
		dbSsessions[c.Value] = session{
			login: login,
			name:  name,
			time:  time.Now(),
		}
		//store user in db
		bs, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		u := user{
			userID.String(),
			name,
			bs,
			nil,
		}
		dbUsers[userID.String()] = u
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
}

func (h *Handler) GetTZFromSingleUser(w http.ResponseWriter, r *http.Request) {
	u := getUser(w, r)
	var zones []string
	rows, err := h.DB.Query("SELECT TZ FROM timeZones WHERE UUID = ?", u.UUID)
	if err != nil {
		log.Fatalln("Error getting TZ: ", err)
	}
	rows.Scan(zones)
	rows.Close()
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(zones)

}
func (h *Handler) CreateTZ(w http.ResponseWriter, r *http.Request) {
	var u user
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "error parsing request", http.StatusInternalServerError)
		return
	}
	checkJson(w, body)
	err = json.Unmarshal(body, u)
	if err != nil {
		http.Error(w, "error unmarshalling", http.StatusInternalServerError)
		return
	}
	checkUser := getUser(w, r)
	if u.UUID == checkUser.UUID {
		prepared, err := h.DB.Prepare("INSERT INTO timeZones (`TZ`, `UUID`) VALUES (?,?)")
		if err != nil {
			log.Fatalln("Error preparing statement for db: ", err)
		}
		_, err = prepared.Exec(u.Tz, u.UUID)
		if err != nil {
			log.Fatalln("Put exe err: ", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
	}
}

func (h *Handler) UpdateTZ(w http.ResponseWriter, r *http.Request) {
	var u user
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "error parsing request", http.StatusInternalServerError)
		return
	}
	checkJson(w, body)
	err = json.Unmarshal(body, u)
	if err != nil {
		http.Error(w, "error unmarshalling", http.StatusInternalServerError)
		return
	}
	checkUser := getUser(w, r)
	if u.UUID == checkUser.UUID {
		_, err := h.DB.Exec("UPDATE timeZones SET "+"TZ=?"+"UUID=?", u.Tz, u.UUID)
		if err != nil {
			log.Fatalln("Error getting TZ: ", err)
		}
	}
}
func (h *Handler) DeleteTZ(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uuid := vars["uuid"]
	result, err := h.DB.Exec(
		"DELETE FROM timeZones WHERE UUID = ?",
		uuid,
	)
	if err != nil {
		log.Fatalln("error deleting: ", err)
	}
	fmt.Println(result)

}

func RefreshAndCreateTable(db *sql.DB) {
	command := []string{
		`DROP TABLE IF EXISTS timeZones;`,
		`CREATE TABLE timeZones (
		  id int NOT NULL AUTO_INCREMENT,
		  uuid text NOT NULL,
		  TZ text NOT NULL,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
	}
	for _, q := range command {
		_, err := db.Exec(q)
		if err != nil {
			panic(err)
		}
	}
}
