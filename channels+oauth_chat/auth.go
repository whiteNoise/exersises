package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

type authHandler struct {
	next http.Handler
}

func (h *authHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if _, err := r.Cookie("auth"); err == http.ErrNoCookie {
		//no auth cookie
		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusTemporaryRedirect)
	} else if err != nil {
		log.Fatal(err)
	} else {
		h.next.ServeHTTP(w, r)
	}
}

//simple handler creating and wrapping
func MustAuth(handler http.Handler) http.Handler {
	return &authHandler{next: handler}
}

// loginHandler handles the third-party login process.
// format: /auth/{action}/{provider}
func handleLogin(w http.ResponseWriter, r *http.Request) {
	segments := strings.Split(r.URL.Path, "/")
	action := segments[2]
	provider := segments[3]
	switch action {
	case "login":
		log.Println("TODO", provider)
	default:
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "Authentification action %s is not supported", action)
	}
}
