package trace

import (
	"fmt"
	"io"
)

//testing interface
type Tracer interface {
	Trace(...interface{})
}
type tracer struct {
	out io.Writer
}
type nilTracer struct{}

func (t *nilTracer) Trace(a ...interface{}) {}
func Off() Tracer {
	return &nilTracer{}
}

func New(w io.Writer) Tracer {
	return &tracer{out: w}
}
func (t *tracer) Trace(input ...interface{}) {
	t.out.Write([]byte(fmt.Sprint(input...)))
	t.out.Write([]byte("\n"))
}
