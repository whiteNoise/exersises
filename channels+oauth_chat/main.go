package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"text/template"

	"./trace"
)

//template struct
type templateHandler struct {
	once     sync.Once
	filename string
	templ    *template.Template
}

//HTTP request handler
func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.once.Do(func() {
		t.templ = template.Must(template.ParseFiles(filepath.Join("templates", t.filename)))
	})
	t.templ.Execute(w, r)
}
func main() {
	var addr = flag.String("addr", ":8080", "server addr")
	flag.Parse()
	r := newRoom()
	r.tracer = trace.New(os.Stdout)
	http.Handle("/chat", MustAuth(&templateHandler{filename: "chat.html"}))
	//dont need redirection so we dont use MustAuth
	http.Handle("/login", &templateHandler{filename: "login.html"})
	http.HandleFunc("/auth/", handleLogin)
	http.Handle("/room", r)

	go r.run()
	log.Println("starting app on: ", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("Error in ListenAndServe: ", err)
	}
}
