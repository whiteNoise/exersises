package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var oauthConfig = &oauth2.Config{
	RedirectURL:  "http://localhost:8080/callback",
	ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
	ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
	Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
	Endpoint:     google.Endpoint,
}
var oauthState = "some random"
var htmlIndex = `<html>
<body>
	<a href="/login">Login form</a>
</body>
</html>`

func main() {
	http.HandleFunc("/", printIndex)
	http.HandleFunc("/login", googleLogin)
	http.HandleFunc("/callback", googleCallback)
	http.ListenAndServe(":8080", nil)

}
func googleLogin(w http.ResponseWriter, r *http.Request) {
	URL := oauthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, URL, http.StatusTemporaryRedirect)
}

func googleCallback(w http.ResponseWriter, r *http.Request) {
	content, err := getInfo(r.FormValue("state"), r.FormValue("code"))
	if err != nil {
		fmt.Println(err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	fmt.Fprintf(w, "Content: %s\n", content)
}
func getInfo(state string, num string) ([]byte, error) {
	if state != oauthState {
		return nil, fmt.Errorf("wrong state ")
	}
	token, err := oauthConfig.Exchange(oauth2.NoContext, num)
	if err != nil {
		return nil, fmt.Errorf("wrong token &s", err.Error)
	}
	googleAPIresponse, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("cannot getting user info %s", err.Error())
	}
	defer googleAPIresponse.Body.Close()
	content, err := ioutil.ReadAll(googleAPIresponse.Body)
	if err != nil {
		return nil, fmt.Errorf("frong responce &s", err.Error)
	}
	return content, nil
}
func printIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, htmlIndex)
}
