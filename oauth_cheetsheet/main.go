package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/stretchr/gomniauth"
	"github.com/stretchr/objx"
)

/*
OAuth2.0 cheatsheet using gominauth library
*/

func loginHandle(w http.ResponseWriter, r *http.Request) {
	segments := strings.Split(r.URL.Path, "/")
	provider := segments[3]
	action := segments[2]
	switch action {
	case "login":
		provider, err := gomniauth.Provider(provider)
		if err != nil {
			log.Fatalln("Error getting provider", provider, " ", err)
		}
		URL, err := provider.GetBeginAuthURL(nil, nil)
		if err != nil {
			log.Fatalln("Error getting auth URL", provider, " ", err)
		}
		w.Header().Set("Location", URL)
		w.WriteHeader(http.StatusTemporaryRedirect)
	case "callback":
		provider, err := gomniauth.Provider(provider)
		if err != nil {
			log.Fatalln("Error when trying to get provider", provider, " ", err)
		}
		creds, err := provider.CompleteAuth(objx.MustFromURLQuery(r.URL.RawQuery))
		if err != nil {
			log.Fatalln("Error when trying to complete auth for",
				provider, "-", err)
		}
		user, err := provider.GetUser(creds)
		if err != nil {
			log.Fatalln("Error when trying to get user from", provider, " ", err)
		}
		authCookie := objx.New(map[string]interface{}{
			"name": user.Name(),
		}).MustBase64()
		http.SetCookie(w, &http.Cookie{
			Name:  "auth",
			Value: authCookie,
			Path:  "/"})

		w.Header()["Location"] = []string{"/chat"}
		w.WriteHeader(http.StatusTemporaryRedirect)
	default:
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "Auth method %s is not supported", action)
	}
}
func main() {

}
