package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func main() {
	loc := "America/New_York"
	tloc, _ := time.LoadLocation(loc)
	fmt.Println(time.Now().In(tloc).Format("2006-01-02T15:04 MST"))
	http.HandleFunc("/", write)
	http.HandleFunc("/get", read)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)

}
func write(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("first-cookie")
	if err == http.ErrNoCookie {
		cookie = &http.Cookie{
			Name:  "first-cookie",
			Value: "0",
		}
	}
	temp, _ := strconv.Atoi(cookie.Value)
	temp++
	cookie.Value = strconv.Itoa(temp)
	http.SetCookie(w, cookie)
}

func read(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("first-cookie")
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
	fmt.Println("your cookie", cookie)
}
